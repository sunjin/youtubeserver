# youtube download server
youtubeをダウンロードするAPIです

# token  
/movie/download/{id}  
/music/download/{id}  
は、jwt tokenがなければ失敗するように設定しています

Headerに
```
Authorization (token)  
User-ID (user)  
```
を設定する  
// TODO  
DBを作ってそこで認証できるようにする、現在はハードコーディングしている


# API
## healthチェック
/
```
{
    "result": "Youtube Download Server!",
    "success": true
}
```

# youtubeの動画ダウンロードURLを返す
/movie/download/{id}
```
{
    "result": {
        "download_path": "download/{movie-file-name}.mp4",
        "suffix": "mp4",
        "title": "{movie-title}"
    },
    "success": true
}
```

# youtubeの動画を音楽ファイルダウンロードURLを返す  
/music/download/{id}  
```
{
    "result": {
        "download_path": "download/{movie-file-name}.mp3",
        "suffix": "mp3",
        "title": "{movie-title}"
    },
    "success": true
}
```

# 実際の動画のダウンロード 
↑のresponseにpathが含まれています10分以内にダウンロードしない場合は、削除されます

/download/\*.mp4  
/download/\*.mp3
```
movie download!
```

# ログイン
/login  (POST)  

param
```
{
    user: "sunjin",
    pass: "alma",
}
```

response 
```
{
    result : {
        "token" : "aaaaaaaaaaaa",
    }
}
```


path  
/music/*  
/movie/*  
のAPIを使用するためには、tokenを発行する必要があります


# compile
make

# run
./run.sh

# docker
docker ディレクトリに入っています

# TODO
youtube-dlコマンドのアップデートを走らせる  


logをこれに置き換えて、デバッグしやすいようにする  
https://qiita.com/kmtr/items/406073651d7a12aab9c6
