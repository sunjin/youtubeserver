package main

import (
	"log"
	"time"

	"github.com/stretchr/graceful"
	"gitlab.com/sunjin/youtubeserver/pkg/cleaner"
	"gitlab.com/sunjin/youtubeserver/pkg/config"
	"gitlab.com/sunjin/youtubeserver/pkg/router"
	"gitlab.com/sunjin/youtubeserver/pkg/updater"
)

func main() {

	// env config
	config.InitEnv()
	env := config.GetEnv()

	// log config
	config.LogRegister()

	// addr
	addr := env.Addr

	// cleaner job
	cleaner.DownloadFileCleaner()

	// updater job
	updater.YoutubeDlCmdUpdater()

	// server
	// get mux
	mux := router.Router()

	log.Println("===== youtube download server start =====")
	log.Println("=== port is ", addr)
	graceful.Run(addr, 1*time.Second, mux)
	log.Println("===== youtube download server stop =====")

}
