package response

import (
	"encoding/json"
	"net/http"
)

// JSON responseをjsonにする
func JSON(w http.ResponseWriter, result interface{}) {

	// make response
	resultMap := map[string]interface{}{
		"result":  result,
		"success": true,
	}

	Respond(w, http.StatusOK, resultMap)
}

// OK 処理が成功したことだけを通知
func OK(w http.ResponseWriter) {
	Respond(w, http.StatusOK, map[string]interface{}{
		"success": true,
	})
}

// FAILED 処理が失敗したときに通知
func FAILED(w http.ResponseWriter) {
	Respond(w, http.StatusNotFound, map[string]interface{}{
		"success": false,
	})
}

// ERROR errorを添えて返す
func ERROR(w http.ResponseWriter, reason string) {
	// make resun
	resultMap := map[string]interface{}{
		"success": false,
		"reason":  reason,
	}
	Respond(w, http.StatusNotFound, resultMap)
}

// Respond 実際に書き込み
func Respond(w http.ResponseWriter, status int, data interface{}) {
	w.WriteHeader(status)
	if data != nil {
		_ = encodeBody(w, data)
	}
}

// encodeBody 実際にBodyに書き込む
func encodeBody(w http.ResponseWriter, data interface{}) error {
	return json.NewEncoder(w).Encode(data)
}
