package param

import (
	"encoding/json"
	"io"
	"log"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

// Map 取得したparamをmap[string]stringで返す
func Map(r *http.Request) map[string]string {
	return mux.Vars(r)
}

// Value 指定したkeyに対応するvalueを取得する
func Value(r *http.Request, key string) string {
	return Map(r)[key]
}

// JSON 受け取ったJsonRequestをパースして構造体に入れる
func JSON(r *http.Request, jsonBody interface{}) error {

	// check Content-Type // TODO utf-8;も含めても大丈夫な判定にする
	// if r.Header.Get("Content-Type") != "application/json" {
	// 	log.Printf("Content-Type : %v", r.Header.Get("Content-Type"))
	// 	return errors.New("Content-Type is not [application/json]")
	// }

	// check Content-Length
	length, err := strconv.Atoi(r.Header.Get("Content-Length"))
	if err != nil {
		log.Println("Content-Length err")
		return err
	}

	// Read body data to parse json
	body := make([]byte, length)
	length, err = r.Body.Read(body)

	if err != nil && err != io.EOF {
		log.Println("Read Body Error")
		return err
	}

	// parse json
	// var jsonBody map[string]interface{}
	err = json.Unmarshal(body[:length], &jsonBody)

	if err != nil {
		log.Println("Json parse error")
		return err
	}

	return nil
}
