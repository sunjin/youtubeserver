package common

import (
	"encoding/base64"
)

// EncodeBase64 base64でencode
func EncodeBase64(str string) string {
	return base64.StdEncoding.EncodeToString([]byte(str))
}
