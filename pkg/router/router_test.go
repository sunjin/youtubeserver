package router_test

import (
	"bytes"
	"encoding/json"
	"log"
	"net/http/httptest"
	"strconv"
	"testing"
	"time"

	"gitlab.com/sunjin/youtubeserver/pkg/auth"
	"gitlab.com/sunjin/youtubeserver/pkg/router"
)

// TestTopHandler topの応答をテストする、tokenを設定指定なので失敗する
func TestFailedTopHandler(t *testing.T) {

	r := router.Router()

	req := httptest.NewRequest("GET", "/", nil)
	rec := httptest.NewRecorder()

	r.ServeHTTP(rec, req)

	t.Log(rec.Body.String())
}

func TestSuccessTopHandler(t *testing.T) {

	// TOP tokenを取得して、tokenをセットした状態でrequestする
	token, err := auth.Generate("sunjin", time.Now())

	if err != nil {
		log.Fatalln("error : ", err)
	}

	r := router.Router()

	req := httptest.NewRequest("GET", "/", nil)

	// token
	req.Header.Set("Authorization", token)
	req.Header.Set("User-ID", "sunjin")

	rec := httptest.NewRecorder()

	r.ServeHTTP(rec, req)
	t.Log(rec.Body.String())

}

func TestLoginHandler(t *testing.T) {

	r := router.Router()

	loginParam := &auth.LoginParam{
		User: "sunjin",
		Pass: "alma",
	}

	b, err := json.Marshal(loginParam)

	if err != nil {
		t.Fatal(err)
	}

	req := httptest.NewRequest("POST", "/login", bytes.NewBuffer(b))
	req.Header.Set("Content-Type", "application/json; charset=UTF-8")
	req.Header.Set("Content-Length", strconv.Itoa(len(b)))
	req.Header.Set("Authorization", "true")
	req.Host = "localhost"

	rec := httptest.NewRecorder()

	r.ServeHTTP(rec, req)
	t.Log(rec.Body.String())
}
