package router

import (
	"fmt"
	"log"
	"net/http"
	"regexp"
	"strings"

	"gitlab.com/sunjin/youtubeserver/pkg/auth"
	"gitlab.com/sunjin/youtubeserver/pkg/config"
)

// corsMiddleware 違うドメインからもアクセスできるようにする
func corsMiddleware(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {

	env := config.GetEnv()

	// CORS
	w.Header().Set("Access-Control-Allow-Origin", env.AllowOrigin)

	//認証を行う
	w.Header().Set("Access-Control-Allow-Credentials", "true")
	w.Header().Set("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization, User-ID")
	// //必要なメソッドを許可する
	w.Header().Set("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS")

	//XMLHttpRequest Level2のpreflightをチェック
	if r.Method == "OPTIONS" {
		//ヘッダーにAuthorizationが含まれていた場合はpreflight成功
		s := r.Header.Get("Access-Control-Request-Headers")
		if strings.Contains(s, "authorization") || strings.Contains(s, "Authorization") {
			w.WriteHeader(http.StatusNoContent)
		} else {
			log.Println("Error...")
			w.WriteHeader(http.StatusBadRequest)
		}
		return
	}

	next(w, r)

}

// match pattern finder
var downloadPathFinderMaker = func() func(string) bool {
	re := regexp.MustCompile(`^/download/*`)

	return func(str string) bool {
		// Copy regexp.
		re := re.Copy()
		return re.MatchString(str)
	}
}

// downloadMiddleware ダウンロードのHeaderを書き換える
func downloadMiddleware(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {

	// 強制ダウンロードにさせる
	finder := downloadPathFinderMaker()
	if finder(r.URL.String()) {

		// fineName を決める
		fileName := r.URL.Query().Get("name")
		w.Header().Set("Content-Type:", "application/force-download")
		w.Header().Set("Content-disposition", fmt.Sprintf("attachment;filename=%s", fileName))
	}

	next(w, r)

}

var loginPathFinderMaker = func() func(string) bool {
	re := regexp.MustCompile(`^/login`)

	return func(str string) bool {
		// Copy regexp
		re := re.Copy()
		return re.MatchString(str)
	}
}

// authMiddleware jwtによる認証check
func authMiddleware(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {

	// r.HeaderにAuth
	authorization := r.Header.Get("Authorization")

	// userid
	userID := r.Header.Get("User-ID")

	// atth
	err := auth.Authenticate(userID, authorization)

	if err != nil {
		log.Println("err: ", err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	next(w, r)
}
