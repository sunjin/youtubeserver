package router

import (
	"net/http"

	"github.com/codegangsta/negroni"
	"github.com/gorilla/mux"
	"gitlab.com/sunjin/youtubeserver/pkg/auth"
	"gitlab.com/sunjin/youtubeserver/pkg/service"
)

// Router .
func Router() *negroni.Negroni {

	n := negroni.New()

	// middleware
	n.Use(negroni.HandlerFunc(corsMiddleware))
	n.Use(negroni.HandlerFunc(downloadMiddleware))

	// log and recovery
	n.Use(negroni.NewLogger())
	n.Use(negroni.NewRecovery())

	router := mux.NewRouter()

	// top
	router.HandleFunc("/", service.Top).Methods("GET")

	// auth
	router.HandleFunc("/singup", auth.Signup).Methods("POST")
	router.HandleFunc("/login", auth.Login).Methods("POST")

	// service
	// movie
	movieSubRouter("/movie", router)

	// music
	musicSubRouter("/music", router)

	// download
	router.PathPrefix("/download/").Handler(http.StripPrefix("/download/", http.FileServer(http.Dir("./download"))))

	n.UseHandler(router)

	return n
}

// movieSubRouter 動画ダウンロードのsubrouter
func movieSubRouter(pathPrefix string, router *mux.Router) {

	movieBaseRouter := mux.NewRouter()
	router.PathPrefix(pathPrefix).Handler(negroni.New(
		negroni.HandlerFunc(authMiddleware),
		negroni.Wrap(movieBaseRouter),
	))
	movieRouter := movieBaseRouter.PathPrefix(pathPrefix).Subrouter()

	// path
	movieRouter.Path("/download/{id}").HandlerFunc(service.MovieDownload).Methods("GET")
}

// musicSubRouter 音楽ダウンロードのsubrouter
func musicSubRouter(pathPrefix string, router *mux.Router) {
	musicBaseRouter := mux.NewRouter()
	router.PathPrefix(pathPrefix).Handler(negroni.New(
		negroni.HandlerFunc(authMiddleware),
		negroni.Wrap(musicBaseRouter),
	))
	musicRouter := musicBaseRouter.PathPrefix(pathPrefix).Subrouter()

	// path
	musicRouter.Path("/download/{id}").HandlerFunc(service.MusicDownload).Methods("GET")
}
