package auth

import (
	"errors"
	"fmt"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
	"gitlab.com/sunjin/youtubeserver/pkg/config"
)

// Auth 証明前の認証トークン情報
type Auth struct {
	UserID string
	Iat    int64
}

// NewAuth authのnew
func NewAuth(userID string, iat int64) *Auth {
	return &Auth{
		UserID: userID,
		Iat:    iat,
	}
}

// secret ハッシュ
var secret = config.GetEnv().TokenSecret

const (
	// user id
	userIDKey = "user_id"

	// iat Issued At, exp expiration time
	iatKey = "iat"
	expKey = "exp"

	// 有効期限
	lifeTime = 30 * time.Minute
)

// Generate JWT tokenを生成
func Generate(userID string, now time.Time) (string, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		userIDKey: userID,
		iatKey:    now.Unix(),
		expKey:    now.Add(lifeTime).Unix(),
	})

	return token.SignedString([]byte(secret))
}

// Parse は jwt tokenの認証情報を取得する
func Parse(signedString string) (*Auth, error) {

	token, err := jwt.Parse(signedString, func(token *jwt.Token) (interface{}, error) {

		if _, ok := token.Method.(*jwt.SigningMethodHMAC); ok {
			return []byte(secret), nil
		}

		// error
		msg := fmt.Sprintf("unexpected signing method: %v\n", token.Header["alg"])
		return "", errors.New(msg)
	})

	// error check
	if err != nil {
		if ve, ok := err.(*jwt.ValidationError); ok {
			if ve.Errors&jwt.ValidationErrorExpired != 0 {
				// expire
				msg := fmt.Sprintf("%s is expired\n err : %v\n", signedString, err.Error())
				return nil, errors.New(msg)
			}
		}
		// invalid
		msg := fmt.Sprintf("%s is invalid\n err : %v\n", signedString, err.Error())
		return nil, errors.New(msg)
	}

	// token check
	if token == nil {
		msg := fmt.Sprintf("not found token in %s", signedString)
		return nil, errors.New(msg)
	}

	claims, ok := token.Claims.(jwt.MapClaims)

	// caims check
	if !ok {
		msg := fmt.Sprintf("not found claims in %s", signedString)
		return nil, errors.New(msg)
	}

	// userID check
	userID, ok := claims[userIDKey].(string)
	if !ok {
		msg := fmt.Sprintf("not found %s in %s", userIDKey, signedString)
		return nil, errors.New(msg)
	}

	iat, ok := claims[iatKey].(float64)
	if !ok {
		msg := fmt.Sprintf("not found %s in %s", iatKey, signedString)
		return nil, errors.New(msg)
	}

	return NewAuth(userID, int64(iat)), nil
}

// Authenticate 正しい認証Tokenであるかを確認
func Authenticate(userID, authorization string) error {

	auth, err := Parse(authorization)

	if err != nil {
		return fmt.Errorf("%w \n failed to parse authorization", err)
	}

	if userID != auth.UserID {
		return fmt.Errorf("failed authentication, userID=%s, userIDPayload=%s", userID, auth.UserID)
	}

	// TODO token発酵後にパスワードが更新されていたら error

	return nil
}
