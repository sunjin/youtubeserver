package auth

import (
	"fmt"
	"log"
	"net/http"
	"time"

	"gitlab.com/sunjin/youtubeserver/pkg/param"
	"gitlab.com/sunjin/youtubeserver/pkg/response"
)

// LoginParam ログインに必要な構造体
type LoginParam struct {
	User string `json:"user"`
	Pass string `json:"pass"`
}

// Signup .
func Signup(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Signup 処理")
	response.JSON(w, "Signup")
}

// Login .
func Login(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Login 処理")

	// get data
	var loginParam LoginParam
	err := param.JSON(r, &loginParam)

	// check
	if err != nil {
		log.Println("error:", err)
		response.ERROR(w, err.Error())
		return
	}

	// judge
	if !judgeLogin(loginParam) {
		log.Println("Login Failed")
		response.ERROR(w, "Failed Login")
		return
	}

	// get token
	// user nameを idとして見る
	userID := loginParam.User
	token, err := Generate(userID, time.Now())

	if err != nil {
		log.Println("Token Error")
		log.Println(err)
		response.ERROR(w, "Token Error")
		return
	}

	resultMap := make(map[string]interface{})
	resultMap["token"] = token

	response.JSON(w, resultMap)
}

// TODO DB judgeLogin ログイン合否処理
func judgeLogin(login LoginParam) bool {
	return login.User == "sunjin" && login.Pass == "alma"
}
