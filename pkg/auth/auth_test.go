package auth_test

import (
	"testing"
	"time"

	"gitlab.com/sunjin/youtubeserver/pkg/auth"
)

func TestCreateToken(t *testing.T) {
	token, err := auth.Generate("sunjin", time.Now())

	if err != nil {
		t.Log("error:", err)
		panic(err)
	}

	t.Log("token:", token)
}

// func TestParseToken(t *testing.T) {

// 	authStruct, err := auth.Parse("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE1NzUzNjU4MzgsImlhdCI6MTU3NTM2NDAzOCwidXNlcl9pZCI6InN1bmppbiJ9.0_AvPV-kLUp5gxLHOu7tLACBE_L9js5uBnlXHPah3XM")

// 	if err != nil {
// 		t.Log("error:", err)
// 		panic(err)
// 	}

// 	t.Log("result:", authStruct)

// }

// func TestAuthenticate(t *testing.T) {
// 	err := auth.Authenticate("sunjin", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE1NzUzNjU4MzgsImlhdCI6MTU3NTM2NDAzOCwidXNlcl9pZCI6InN1bmppbiJ9.0_AvPV-kLUp5gxLHOu7tLACBE_L9js5uBnlXHPah3XM")

// 	if err != nil {
// 		t.Log("error:", err)
// 		panic(err)
// 	}

// 	t.Log("OK!")
// }
