package cleaner

import (
	"io/ioutil"
	"log"
	"os"

	"path/filepath"
	"time"

	"github.com/bamzi/jobrunner"
	filetime "gopkg.in/djherbis/times.v1"
)

// DownloadFileCleaner ダウンロードファイルの場所を定期的にクリーンする
func DownloadFileCleaner() {
	jobrunner.Start()
	jobrunner.Every(30*time.Second, CleanJob{})
}

// 10分で削除
const (
	deleteDuration    time.Duration = 600 * time.Second
	downloadDirectory string        = "download"
)

// CleanJob .
type CleanJob struct {
}

// Run ダウンロードファイルクリーン処理
func (CleanJob) Run() {
	paths, err := dirwalk(downloadDirectory)

	if err != nil {
		log.Println("[Cleaner] Error ! err:", err)
		return
	}

	for _, path := range paths {
		changeTime, err := getFileChangeTime(path)

		if err != nil {
			log.Println("[Cleaner] Error:", err)
			return
		}
		if judgeDeleteTime(deleteDuration, changeTime) {
			log.Printf("[Cleaner] file delete -> %s", path)

			if err := os.Remove(path); err != nil {
				log.Println("[Cleaner] 削除に失敗しました。err:", err)
				return
			}
		}
	}
}

// getFileStatusTime fileの時間情報をとにかく取得してくれる
func getFileChangeTime(path string) (time.Time, error) {

	t, err := filetime.Stat(path)
	if err != nil {
		return time.Now(), err
	}

	return t.ChangeTime(), nil
}

// judgeDeleteTime 削除するかどうかの判定
func judgeDeleteTime(judgeDuration time.Duration, changeTime time.Time) bool {

	// 現在の時間
	nowTime := time.Now()

	// 差分Duration比較
	diffMill := getDiffMillseconds(changeTime, nowTime)

	judgeMill := judgeDuration.Milliseconds()

	return judgeMill < diffMill
}

// getDiffDuratin 差分を取得
func getDiffMillseconds(beforeTime, afterTime time.Time) int64 {
	beforeMs := getMs(beforeTime)
	afterMs := getMs(afterTime)

	return afterMs - beforeMs
}

// getMs msに変換
func getMs(t time.Time) int64 {
	utc := t.UTC()
	return utc.UnixNano() / int64(time.Millisecond)
}

// dirwalk file探索
func dirwalk(dir string) ([]string, error) {
	files, err := ioutil.ReadDir(dir)

	if err != nil {
		return nil, err
	}

	var paths []string

	for _, file := range files {
		if file.IsDir() {

			nextDir, err := dirwalk(filepath.Join(dir, file.Name()))

			if err != nil {
				return nil, err
			}

			paths = append(paths, nextDir...)
			continue
		}
		paths = append(paths, filepath.Join(dir, file.Name()))
	}

	return paths, nil
}
