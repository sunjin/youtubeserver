package service

import (
	"errors"
	"fmt"
	"log"
	"path/filepath"
	"strings"

	"net/http"

	"gitlab.com/sunjin/youtubeserver/pkg/common"
	"gitlab.com/sunjin/youtubeserver/pkg/constant"
	"gitlab.com/sunjin/youtubeserver/pkg/param"
	"gitlab.com/sunjin/youtubeserver/pkg/response"
	"gitlab.com/sunjin/youtubeserver/pkg/youtube"
)

// Top helth check
func Top(w http.ResponseWriter, r *http.Request) {
	response.JSON(w, "Youtube Download Server!")
}

const (
	downloadDirectory = "download"
)

// 他に拡張子があれば随時追加
var movieSuffix = []string{"mp4", "m4a"}
var musicSuffix = []string{"mp3"}

// MovieDownload 動画をダウンロードさせる
func MovieDownload(w http.ResponseWriter, r *http.Request) {
	download(w, r, constant.KindMovie, movieSuffix)
}

// MusicDownload 音楽をダウンロードさせる
func MusicDownload(w http.ResponseWriter, r *http.Request) {
	download(w, r, constant.KindMusic, musicSuffix)
}

// execDownload 実際にダウンロードする
func execDownload(videoInfo *youtube.VideoInfo, dir string, kind string) error {

	switch kind {
	case constant.KindMovie:
		return videoInfo.MovieDownload(dir)

	case constant.KindMusic:
		return videoInfo.MusicDownload(dir)

	default:
		return errors.New("指定されたkindは実装されていません")
	}

}

// download youtubeの動画をdownloadする
func download(w http.ResponseWriter, r *http.Request, kind string, suffixs []string) {

	// urlを取得する
	id := param.Value(r, "id")
	log.Println("ID : ", id)

	videoInfo, err := youtube.GetVideoInfoFromID(id)

	if err != nil {
		// idから動画データを取得できませんでした
		response.ERROR(w, err.Error())
		return
	}

	// すでにあるか確認
	downloadPath, err := getDownloadPath(videoInfo.Title, suffixs)
	if err != nil {
		response.ERROR(w, err.Error())
		return
	}

	if downloadPath != "" {
		log.Println("すでにダウンロード済みコンテンツ")
		response.JSON(w, map[string]string{
			"title":         videoInfo.Title,
			"download_path": downloadPath,
			"suffix":        getSuffix(downloadPath),
		})
		return
	}

	// download
	err = execDownload(videoInfo, downloadDirectory, kind)

	if err != nil {
		// 動画のダウンロードに失敗しました
		response.ERROR(w, errors.New("動画のダウンロードに失敗しました").Error())
		return
	}

	// download path取得
	downloadPath, err = getDownloadPath(videoInfo.Title, suffixs)

	if err != nil {
		response.ERROR(w, err.Error())
		return
	}

	// make result
	resultMap := map[string]string{
		"title":         videoInfo.Title,
		"download_path": downloadPath,
		"suffix":        getSuffix(downloadPath),
	}

	response.JSON(w, resultMap)
}

// getDownloadURL 実際にダウンロードできるurlを取得する
func getDownloadPath(title string, suffixs []string) (string, error) {

	encTitle := common.EncodeBase64(title)

	pattern := fmt.Sprintf("./%s/%s.*", downloadDirectory, encTitle)

	fileName, err := getPatternPath(pattern, suffixs)

	if err != nil {
		return "", err
	}

	return fileName, nil
}

func getPatternPath(pattern string, suffixList []string) (string, error) {
	files, err := filepath.Glob(pattern)

	if err != nil {
		return "", err
	}

	// マッチしたパスがない場合
	if files == nil {
		return "", nil
	}

	for _, file := range files {
		suffix := getSuffix(file)
		log.Println("suffix:", suffix)
		if contains(suffixList, suffix) {
			log.Println("found！", suffix)
			return file, nil
		}
	}

	return "", nil
}

// getSuffix 拡張子を取得
func getSuffix(file string) string {
	pos := strings.LastIndex(file, ".") + 1
	return file[pos:]
}

func contains(list []string, tgt string) bool {
	for _, str := range list {
		if str == tgt {
			return true
		}
	}
	return false
}
