package config

import (
	"github.com/kelseyhightower/envconfig"
)

// Env 環境変数を取得
type Env struct {
	Addr        string `envconfig:"YOUTUBE_SERVER_ADDRESS" default:":9000"`
	AllowOrigin string `envconfig:"ACCESS_CONTROL_ALLOW_ORIGIN" default:"*"`
	TokenSecret string `envconfig:"SIGNINGKEY" default:"aaaa"`
}

var env Env

// GetEnv 環境変数を取得する
func GetEnv() *Env {
	return &env
}

// InitEnv 環境変数を同録
func InitEnv() {
	envconfig.Process("", &env)
}
