package config

import (
	"log"

	"github.com/comail/colog"
)

// LogRegister ログの設定
func LogRegister() {
	// logsetting
	colog.SetDefaultLevel(colog.LDebug)
	colog.SetMinLevel(colog.LTrace)
	colog.SetFormatter(&colog.StdFormatter{
		Colors: true,
		Flag:   log.Ldate | log.Ltime | log.Lshortfile,
	})
	colog.Register()
}
