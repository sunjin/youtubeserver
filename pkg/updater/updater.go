package updater

import (
	"log"
	"os/exec"
	"time"

	"github.com/bamzi/jobrunner"
)

// YoutubeDlCmdUpdater youtube-dlコマンドを定期的にアップデートしてあげる
func YoutubeDlCmdUpdater() {
	jobrunner.Start()
	jobrunner.Every(86400*time.Second, UpdaterJob{})
}

// UpdaterJob .
type UpdaterJob struct {
}

// Run 走るJob
func (UpdaterJob) Run() {
	log.Println("[Updater] update check start")

	exec.Command("youtube-dl", "-U").Run()

	log.Println("[Updater] update check end")
}
