package youtube

import (
	"fmt"
	"log"
	"os/exec"

	"github.com/rylio/ytdl"
	"gitlab.com/sunjin/youtubeserver/pkg/common"
)

// VideoInfo 詳細情報
type VideoInfo struct {
	ID    string
	Title string
}

// GetVideoInfoFromURL 詳細情報をURLから取得する
func GetVideoInfoFromURL(youtubeURL string) (*VideoInfo, error) {

	vid, err := ytdl.GetVideoInfo(youtubeURL)

	if err != nil {
		return nil, err
	}

	return &VideoInfo{
		ID:    vid.ID,
		Title: vid.Title,
	}, nil

}

// GetVideoInfoFromID idからデータを取得
func GetVideoInfoFromID(id string) (*VideoInfo, error) {
	vid, err := ytdl.GetVideoInfoFromID(id)

	if err != nil {
		return nil, err
	}

	return &VideoInfo{
		ID:    vid.ID,
		Title: vid.Title,
	}, nil
}

// MovieDownload 動画MP4形式でダウンロード
func (videoInfo *VideoInfo) MovieDownload(dir string) error {
	fileName := videoInfo.makeFileName(dir)

	// make exec
	cmd := exec.Command("youtube-dl", "-f", "bestvideo[ext=mp4]+bestaudio[ext=m4a]", "-o", fileName, videoInfo.ID)
	return execDownload(cmd)

}

// MusicDownload 音楽MP3形式でダウンロード
func (videoInfo *VideoInfo) MusicDownload(dir string) error {
	fileName := videoInfo.makeFileName(dir)
	fileNamePattern := fileName + ".%(ext)s"
	cmd := exec.Command("youtube-dl", "-f", "bestaudio", "--extract-audio", "--audio-format", "mp3", "--audio-quality", "0", "-o", fileNamePattern, videoInfo.ID)
	return execDownload(cmd)
}

// makeFileName ダウンロードしたデータのファイル名を作成
func (videoInfo *VideoInfo) makeFileName(dir string) string {
	title := videoInfo.Title
	encTitle := common.EncodeBase64(title)
	return fmt.Sprintf("./%s/%s", dir, encTitle)
}

// execDownload ダウンロード処理
func execDownload(cmd *exec.Cmd) error {

	log.Println("cmd is ->", cmd.String())

	if err := cmd.Start(); err != nil {
		return err
	}

	if err := cmd.Wait(); err != nil {
		return err
	}

	return nil
}
