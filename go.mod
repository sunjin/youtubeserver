module gitlab.com/sunjin/youtubeserver

go 1.13

replace github.com/Sirupsen/logrus => ./lib/logrus // 名前解決

require (
	github.com/PuerkitoBio/goquery v1.5.0 // indirect
	github.com/Sirupsen/logrus v0.0.0-00010101000000-000000000000 // indirect
	github.com/bamzi/jobrunner v1.0.0
	github.com/codegangsta/negroni v1.0.0
	github.com/comail/colog v0.0.0-20160416085026-fba8e7b1f46c
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gorilla/mux v1.7.3
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/kkdai/youtube v1.0.0
	github.com/rylio/ytdl v0.5.1

	github.com/sirupsen/logrus v1.4.2

	github.com/stretchr/graceful v1.2.15
	github.com/stretchr/testify v1.2.2
	golang.org/x/text v0.3.2 // indirect
	gopkg.in/djherbis/times.v1 v1.2.0
)
