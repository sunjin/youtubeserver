# docker 
マイクロサービスとして配布できるようにDockerImageにしています

portが正しくフォワードしてくれないので悩んでいます  
kubernetesのserviceでフォワードしようかな...

# ffmpeg
動画の拡張子を変換するものです  
下記のサイトから linux用の tar.xzファイルを取得してください  
https://johnvansickle.com/ffmpeg/  
Dockerfileに、解凍して取得したffmpegとffprobeをCOPYしてpathが通っている場所に配置します

# script
## makeImage.sh
docker imageを作成します。

---

## run.sh
docker runです

# 環境変数
YOUTUBE_SERVER_ADDRESS=":9000"
ACCESS_CONTROL_ALLOW_ORIGIN="*"

# jwt token 
SIGNINGKEY="aaaa"