#!/bin/zsh
VERSION="1.3.0"

cd ../
make linux
cd docker

docker build --tag sunjin110/youtubeserver:${VERSION} .
